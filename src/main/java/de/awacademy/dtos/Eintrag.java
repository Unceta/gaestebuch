package de.awacademy.dtos;

public class Eintrag {

    private String name;
    private String eintrag;

    public Eintrag(String name, String eintrag) {
        this.name = name;
        this.eintrag = eintrag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEintrag() {
        return eintrag;
    }

    public void setEintrag(String eintrag) {
        this.eintrag = eintrag;
    }
}

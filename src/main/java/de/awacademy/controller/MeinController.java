package de.awacademy.controller;

import de.awacademy.dtos.Eintrag;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;

import static org.springframework.web.util.HtmlUtils.htmlEscape;

@Controller
public class MeinController {
    private List<Eintrag> eintraege = new LinkedList<>();

    @GetMapping("/")
    @ResponseBody
    public String index() {

        return getEintraege() + getEingabeform();
    }

    @PostMapping("/")
    @ResponseBody
    public String fuegeEintragHinzu(@ModelAttribute Eintrag eintrag) {

        eintraege.add(escapeHtmlEintrag(eintrag));

        return index();
    }


    private String getEingabeform() {
        String htmlOutputNeuerEintrag;
        htmlOutputNeuerEintrag = "<form action=\"/\" method=\"POST\">" +
                "<input name=\"name\" placeholder=\"Dein Name\">" +
                "<input name=\"eintrag\" placeholder=\"Neuer Eintrag\">" +
                "<button>Los</button>" +
                "</form>";

        return htmlOutputNeuerEintrag;
    }

    private String getEintraege() {
        String htmlOutputEintraege = "";

        if(!eintraege.isEmpty()) {
            for (Eintrag e : eintraege) {
                htmlOutputEintraege += e.getName() + "<br>" + e.getEintrag() + "<br><br>";
            }
        }
        return htmlOutputEintraege;
    }

    private Eintrag escapeHtmlEintrag(Eintrag eintrag){
        eintrag.setName(htmlEscape(eintrag.getName()));
        eintrag.setEintrag(htmlEscape(eintrag.getEintrag()));

        return eintrag;
    }

    private String escape(String inputString){
        return inputString
                .replaceAll("<", "&lt")
                .replaceAll(">", "&gt")
                .replaceAll("\"", "&quot");
    }
}
